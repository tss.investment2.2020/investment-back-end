package com.investments.dtos.user;

import com.sun.istack.NotNull;
import lombok.Data;

import javax.persistence.UniqueConstraint;

@Data
public class NewUserDTO {

    @NotNull
    private String name;

    @NotNull
    private String lastName;

    @NotNull
    private String email;

    @NotNull
    private String password;

    private String phone;
}
