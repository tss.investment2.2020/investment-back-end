package com.investments.dtos.user;

import com.sun.istack.NotNull;
import lombok.Data;

@Data
public class NewLoginDTO {

    @NotNull
    private String email;

    @NotNull
    private String password;
}
