package com.investments.dtos.project;

import lombok.Data;

@Data
public class NewInitialDataDTO {

    private String type;

    private Float pessimisticEstimate;

    private Float optimisticEstimate;

    private Float probableEstimate;
}
