package com.investments.dtos.project;

import lombok.Data;

@Data
public class NewRateInflationDTO {
    private String year;

    private Float pessimisticState;

    private Float optimisticState;

    private Float probableState;
}
