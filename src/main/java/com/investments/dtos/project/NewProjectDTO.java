package com.investments.dtos.project;

import com.sun.istack.NotNull;
import lombok.Data;

@Data
public class NewProjectDTO {

    @NotNull
    private String name;

    private String description;
}
