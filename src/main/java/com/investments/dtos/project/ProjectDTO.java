package com.investments.dtos.project;

import com.investments.model.InitialData;
import com.investments.model.RateInflation;
import lombok.Data;

@Data
public class ProjectDTO {

    private Long id;

    private String name;

    private String description;

    private Long userId;

    private InitialData initialData;

    private RateInflation rateInflation;
}
