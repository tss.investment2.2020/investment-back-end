package com.investments.dtos.project;

import lombok.Data;

@Data
public class ProjectsDTO {

    private Long id;

    private String name;

    private String description;

    private Long userId;
}
