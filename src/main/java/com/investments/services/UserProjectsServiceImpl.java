package com.investments.services;

import com.investments.dtos.project.*;
import com.investments.model.InitialData;
import com.investments.model.Project;
import com.investments.repository.UserProjectsRepository;
import com.investments.services.mappers.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.stream.Collectors;


@Service
public class UserProjectsServiceImpl implements UserProjectsService<ProjectDTO, NewProjectDTO> {

    @Autowired
    private UserProjectsRepository<Project> userProjectsRepository;

    @Autowired
    private ProjectDTOMapper projectDTOMapper;

    @Autowired
    private NewProjectDTOMapper newProjectDTOMapper;

    @Autowired
    private ProjectMapper projectMapper;

    @Override
    public Collection<ProjectDTO> getAllProjects(Long userId) {
        return userProjectsRepository.getAll(userId)
                .stream().map(project -> projectDTOMapper.toProjectDTO(project))
                .collect(Collectors.toList());
    }

    @Override
    public ProjectDTO createProject(Long userId, NewProjectDTO newProjectDTO) {
        Project project = newProjectDTOMapper.toProject(newProjectDTO);
        Project savedProject = userProjectsRepository.save(userId, project);
        return projectDTOMapper.toProjectDTO(savedProject);
    }

    @Override
    public ProjectDTO getProject(Long id) {
        return projectDTOMapper.toProjectDTO(userProjectsRepository.get(id));
    }
}
