package com.investments.services;

import java.util.Collection;

public interface UserProjectsService<Type, NewType> {

    Collection<Type> getAllProjects(Long userId);

    Type createProject(Long userId, NewType newType);

    Type getProject(Long id);
}
