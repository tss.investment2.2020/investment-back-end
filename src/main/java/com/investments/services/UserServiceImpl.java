package com.investments.services;

import com.investments.dtos.user.NewUserDTO;
import com.investments.dtos.user.UserDTO;
import com.investments.model.User;
import com.investments.repository.UserRepository;
import com.investments.services.mappers.NewUserDTOMapper;
import com.investments.services.mappers.UserDTOMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
public class UserServiceImpl implements UserService<UserDTO, NewUserDTO> {

    @Autowired
    private UserRepository<User> userRepository;

    @Autowired
    private UserDTOMapper userDTOMapper;

    @Autowired
    private NewUserDTOMapper newUserDTOMapper;

    @Override
    public Collection<UserDTO> getAll() {
        return userRepository.getAll().stream().map(user -> userDTOMapper.toUserDTO(user)).collect(Collectors.toList());
    }

    @Override
    public UserDTO create(NewUserDTO newUserDTO) {
        User user = userRepository.create(newUserDTOMapper.toUser(newUserDTO));
        return userDTOMapper.toUserDTO(user);
    }

    @Override
    public UserDTO getUser(String email) {
        User user = userRepository.getUser(email).orElseThrow();
        return userDTOMapper.toUserDTO(user);
    }

    @Override
    public boolean exists(String email, String password) {
        return userRepository.existUser(email, password);
    }

    @Override
    public UserDTO get(Long id) {
        return userDTOMapper.toUserDTO(userRepository.get(id));
    }
}
