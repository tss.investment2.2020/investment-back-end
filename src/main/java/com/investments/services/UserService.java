package com.investments.services;

import java.util.Collection;

public interface UserService<Type, NewType> {

    Collection<Type> getAll();

    Type create(NewType newType);

    Type getUser(String email);

    boolean exists(String email, String password);

    Type get(Long id);
}
