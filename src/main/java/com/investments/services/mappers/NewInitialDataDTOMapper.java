package com.investments.services.mappers;

import com.investments.dtos.project.NewInitialDataDTO;
import com.investments.model.InitialData;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface NewInitialDataDTOMapper {
    InitialData toInitialData(NewInitialDataDTO newInitialDataDTO);
}
