package com.investments.services.mappers;

import com.investments.dtos.project.NewRateInflationDTO;
import com.investments.model.RateInflation;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface NewRateInflationDTOMapper {
    RateInflation toRateInflation(NewRateInflationDTO newRateInflationDTO);
}
