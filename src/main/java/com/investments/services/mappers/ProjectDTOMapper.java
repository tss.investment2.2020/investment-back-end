package com.investments.services.mappers;


import com.investments.dtos.project.ProjectDTO;
import com.investments.model.Project;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring")
public interface ProjectDTOMapper {

    ProjectDTO toProjectDTO(Project project);
}
