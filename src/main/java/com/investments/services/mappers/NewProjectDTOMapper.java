package com.investments.services.mappers;


import com.investments.dtos.project.NewProjectDTO;
import com.investments.model.Project;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface NewProjectDTOMapper {

    Project toProject(NewProjectDTO newProjectDTO);
}
