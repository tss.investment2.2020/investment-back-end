package com.investments.services.mappers;

import com.investments.dtos.user.NewUserDTO;
import com.investments.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring")
public interface NewUserDTOMapper {

    User toUser(NewUserDTO newUserDTO);
}
