package com.investments.services.mappers;

import com.investments.dtos.project.ProjectDTO;
import com.investments.model.Project;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProjectMapper {

    Project toProject(ProjectDTO projectDTO);
}
