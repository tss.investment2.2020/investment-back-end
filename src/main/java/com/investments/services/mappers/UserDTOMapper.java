package com.investments.services.mappers;

import com.investments.dtos.user.UserDTO;
import com.investments.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring")
public interface UserDTOMapper {

    UserDTO toUserDTO(User user);
}
