package com.investments.repository.jpa;

import com.investments.model.Project;
import com.investments.repository.UserProjectsRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;

public class UserProjectsJPARepositoryImpl implements UserProjectsRepository<Project> {

    @Autowired
    private UserProjectsJPARepository userProjectsJPARepository;


    @Override
    public Collection<Project> getAll(Long userId) {
        return userProjectsJPARepository.findAllByUserId(userId);
    }

    @Override
    public Project save(Long userId, Project project) {
        project.setUserId(userId);
        return userProjectsJPARepository.save(project);
    }

    @Override
    public Project get(Long id) {
        return userProjectsJPARepository.getOne(id);
    }
}
