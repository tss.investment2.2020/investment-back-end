package com.investments.repository.jpa;

import com.investments.model.RateInflation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RateInflationJPARepository extends JpaRepository<RateInflation, Long> {
}
