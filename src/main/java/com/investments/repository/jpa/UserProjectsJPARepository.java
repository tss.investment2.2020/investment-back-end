package com.investments.repository.jpa;

import com.investments.model.Project;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

public interface UserProjectsJPARepository extends JpaRepository<Project, Long> {

    Collection<Project> findAllByUserId(Long userId);
}
