package com.investments.repository.jpa;

import com.investments.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserJPARepository extends JpaRepository<User, Long> {

    Optional<User> findByEmail(String email);

    boolean existsByEmailAndPassword(String email, String password);

    Optional<User> getByEmail(String email);
}
