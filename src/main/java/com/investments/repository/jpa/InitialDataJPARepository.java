package com.investments.repository.jpa;

import com.investments.model.InitialData;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InitialDataJPARepository extends JpaRepository<InitialData, Long> {
}
