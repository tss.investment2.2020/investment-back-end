package com.investments.repository.jpa;

import com.investments.model.User;
import com.investments.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Optional;


@Component
public class UserJPARepositoryImpl implements UserRepository<User> {

    @Autowired
    private UserJPARepository userJPARepository;

    @Override
    public Collection<User> getAll() {
        return userJPARepository.findAll();
    }

    @Override
    public User create(User user) {
        return userJPARepository.save(user);
    }

    @Override
    public Optional<User> getUser(String email) {
        return userJPARepository.getByEmail(email);
    }

    @Override
    public boolean existUser(String email, String password) {
        return userJPARepository.existsByEmailAndPassword(email, password);
    }

    @Override
    public User get(Long id) {
        return userJPARepository.getOne(id);
    }
}
