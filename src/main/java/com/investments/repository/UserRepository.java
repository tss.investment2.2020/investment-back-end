package com.investments.repository;


import com.investments.dtos.user.UserDTO;

import java.util.Collection;
import java.util.Optional;

public interface UserRepository<TypeUser> {

    Collection<TypeUser> getAll();

    Optional<TypeUser> getUser(String email);

    TypeUser create(TypeUser typeUser);

    boolean existUser(String email, String password);

    TypeUser get(Long id);
}
