package com.investments.repository;

import java.util.Collection;

public interface UserProjectsRepository<Type> {

    Collection<Type> getAll(Long userId);

    Type save(Long userId, Type type);

    Type get(Long id);
}
