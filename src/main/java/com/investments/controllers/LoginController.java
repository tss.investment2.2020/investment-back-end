package com.investments.controllers;

import com.investments.dtos.user.NewLoginDTO;
import com.investments.dtos.user.NewUserDTO;
import com.investments.dtos.user.UserDTO;
import com.investments.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/auth")
public class LoginController {

    @Autowired
    private UserService<UserDTO, NewUserDTO> userService;

    @PostMapping("/login")
    public ResponseEntity<UserDTO> login(@Valid @RequestBody NewLoginDTO newLoginDTO) {
        if (userService.exists(newLoginDTO.getEmail(), newLoginDTO.getPassword())) {
            UserDTO userDTO = userService.getUser(newLoginDTO.getEmail());
            return ResponseEntity.ok(userDTO);
        }
        return ResponseEntity.badRequest().build();
    }
}
