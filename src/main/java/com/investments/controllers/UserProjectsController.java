package com.investments.controllers;

import com.investments.dtos.project.*;
import com.investments.model.InitialData;
import com.investments.model.RateInflation;
import com.investments.repository.jpa.InitialDataJPARepository;
import com.investments.repository.jpa.RateInflationJPARepository;
import com.investments.services.UserProjectsService;
import com.investments.services.mappers.NewInitialDataDTOMapper;
import com.investments.services.mappers.NewRateInflationDTOMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;

@RestController
@RequestMapping("/users/{userId}/projects")
public class UserProjectsController {

    @Autowired
    private UserProjectsService<ProjectDTO, NewProjectDTO> userProjectsService;

    @Autowired
    private InitialDataJPARepository initialDataJPARepository;

    @Autowired
    private NewInitialDataDTOMapper newInitialDataDTOMapper;

    @Autowired
    private RateInflationJPARepository rateInflationJPARepository;

    @Autowired
    private NewRateInflationDTOMapper newRateInflationDTOMapper;

    @GetMapping
    public ResponseEntity<Collection<ProjectDTO>> getAll(@PathVariable Long userId) {
        return ResponseEntity.ok(userProjectsService.getAllProjects(userId));
    }

    @PostMapping
    public ResponseEntity<ProjectDTO> create(@PathVariable Long userId, @Valid @RequestBody NewProjectDTO newProjectDTO) {
        return ResponseEntity.ok(userProjectsService.createProject(userId, newProjectDTO));
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProjectDTO> get(@PathVariable Long id) {
        return ResponseEntity.ok(userProjectsService.getProject(id));
    }

    @PostMapping("/{id}/initial-data")
    public ResponseEntity<ProjectDTO> createInitialState(
            @PathVariable Long id, @RequestBody NewInitialDataDTO newInitialDataDTO) {
        InitialData initialData = newInitialDataDTOMapper.toInitialData(newInitialDataDTO);
        initialData.setProjectId(id);
        initialDataJPARepository.save(initialData);
        return ResponseEntity.ok(userProjectsService.getProject(id));
    }

    @PostMapping("/{id}/rate-inflation")
    public ResponseEntity<ProjectDTO> createRateInflation(
            @PathVariable Long id, @RequestBody NewRateInflationDTO newRateInflationDTO) {
        RateInflation rateInflation = newRateInflationDTOMapper.toRateInflation(newRateInflationDTO);
        rateInflation.setProjectId(id);
        rateInflationJPARepository.save(rateInflation);
        return ResponseEntity.ok(userProjectsService.getProject(id));
    }
}
