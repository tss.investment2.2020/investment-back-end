package com.investments.controllers;

import com.investments.dtos.user.NewUserDTO;
import com.investments.dtos.user.UserDTO;
import com.investments.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import java.util.Collection;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService<UserDTO, NewUserDTO> userService;

    @GetMapping
    public ResponseEntity<Collection<UserDTO>> getAll() {
        return ResponseEntity.ok(userService.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDTO> get(@PathVariable Long id) {
        return ResponseEntity.ok(userService.get(id));
    }

    @PostMapping
    public ResponseEntity<UserDTO> create(@Valid @RequestBody NewUserDTO newUserDTO) {
        UserDTO userDTO = userService.create(newUserDTO);
        return ResponseEntity.ok(userDTO);
    }
}
