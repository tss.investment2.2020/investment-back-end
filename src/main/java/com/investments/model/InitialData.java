package com.investments.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.hibernate.annotations.Fetch;

import javax.persistence.*;

@Data
@Entity
@Table(name = "INITIAL_DATA")
public class InitialData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "type")
    private String type;

    @Column(name = "pessimistic_estimate")
    private Float pessimisticEstimate;

    @Column(name = "optimistic_estimate")
    private Float optimisticEstimate;

    @Column(name = "probable_estimate")
    private Float probableEstimate;

    @Column(name = "project_id")
    private Long projectId;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "project_id", insertable = false, updatable = false)
    private Project project;
}
