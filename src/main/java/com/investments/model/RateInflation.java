package com.investments.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "RATE_INFLATION")
public class RateInflation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "year")
    private String year;

    @Column(name = "pessimistic_state")
    private Float pessimisticState;

    @Column(name = "optimistic_state")
    private Float optimisticState;

    @Column(name = "probable_state")
    private Float probableState;

    @Column(name = "project_id")
    private Long projectId;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "project_id", insertable = false, updatable = false)
    private Project project;
}
