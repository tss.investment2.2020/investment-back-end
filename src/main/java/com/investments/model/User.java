package com.investments.model;


import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "USER")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column(name = "lastname")
    private String lastName;

    @Column(unique = true, name = "email")
    private String email;

    @Column
    private String password;

    @Column
    private String phone;
}
