create database investments_db;

use investments_db;

drop table if exists user;
create table `USER` (
	`id` bigint not null auto_increment,
    `name` varchar(20) not null,
    `lastname` varchar(40),
    `email` varchar(40) not null unique,
    `password` varchar(30) not null,
    `phone` varchar(12),
	primary key (`id`)
);